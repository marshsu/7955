<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ImageGallery;
use URL;
use Illuminate\Support\Facades\Response;
use Image;

class ImageGalleryController extends Controller
{
    private $pathPhoto = 'upimages/';
    private $pathPhotoOrign = 'upimages/orign/';

    /**
     * Listing Of images gallery
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        dd("qq");
        $images = ImageGallery::get();
        return view('image-gallery', compact('images'));
    }


    /**
     * Upload image function
     *
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        $this->validate($request, [
            'title' => 'nullable',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:20480',
        ]);

        $imageName = date("ymd_") . time() . '.' . $request->image->getClientOriginalExtension();

        $input['image'] = $imageName;

        // 原圖
        $request->image->move(public_path($this->pathPhotoOrign), $imageName);

        // 縮圖
        $this->savePhotoThumbnail(public_path($this->pathPhotoOrign . $imageName), public_path($this->pathPhoto . $imageName));

        $input['title'] = ($request->title) ? $request->title : '20171231同聚';
        ImageGallery::create($input);

        return back()
            ->with('success', '照片上傳完成.');
    }


    /**
     * Remove Image function
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $errMessage = '不可以刪除!!!!';

        $image = ImageGallery::find($id);//->delete();

        $fromUrl = parse_url(URL::previous());

        if (array_search('act=mars', $fromUrl)) {
            @unlink(public_path($this->pathPhoto . $image->image));
            @unlink(public_path($this->pathPhotoOrign . $image->image));
            $image->delete();
            $errMessage = '刪除成功!!';
        }

        return back()
            ->with('success', $errMessage);
    }

    public function download($id)
    {
        $image = ImageGallery::find($id);

        $image->downloads += 1;
        $image->save();

        $imageFullPath = public_path($this->pathPhotoOrign . $image->image);

        return Response::download($imageFullPath);
    }

    private function savePhotoThumbnail($inFile, $newFileName)
    {
        $imageWidth = 960;
        $imageHeight = 540;
        $inImage = Image::make($inFile);

        if ($inImage->width() != $imageWidth || $inImage->height() != $imageHeight) {
            $inImage->fit($imageWidth, $imageHeight);
        }
        $inImage->save(($newFileName));
    }

}
